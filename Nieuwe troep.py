import easygui as gui
import sys
import json

users = []
passwords = []
status = ""

def displayMenu():
    status = gui.buttonbox("Are you a registered user?", "Login", choices=('Yes', 'No', 'Cancel'))  
    if status == 'Yes':
        oldUser()
    elif status == 'No':
        newUser()
    elif status == 'Cancel':
        sys.exit(0)

def newUser():
    createLogin = gui.enterbox("Create login name")
    if createLogin in users:
        gui.msgbox("Login name already exists!")
    else:
        createPassw = gui.enterbox("Create password")
    

        #Schrijf de credentials weg naar de list#
        users.append(createLogin)
        passwords.append(createPassw)

        #Schrijf de list weg naar de txt files#
        with open('gebruikers.txt', 'w') as f:
            f.write(json.dumps(users))
        with open('wachtwoorden.txt', 'w') as g:
            g.write(json.dumps(passwords))

        gui.msgbox("User created!")
        displayMenu()

def oldUser():
    login = gui.enterbox("Enter login name")
    passw = gui.enterbox("Enter password")
    logintries = 1
   

    #Open de txt bestanden om de credentials uit te lezen#
    with open('gebruikers.txt', 'r') as f:
        users = json.loads(f.read())
    with open('wachtwoorden.txt', 'r') as g:
        passwords = json.loads(g.read())

    if login in users and passw in passwords:
        gui.msgbox("Login successful!")
    #Wachtwoord of gebruikersnaam is verkeerd, vijf pogingen#
    else:
        while logintries < 5:
            gui.msgbox("User doesn't exist or wrong password!")
            login = gui.enterbox("Enter login name")
            passw = gui.enterbox("Enter password")
            logintries = logintries + 1
        gui.msgbox("Maximum amount of login tries reached.")
          
displayMenu()
