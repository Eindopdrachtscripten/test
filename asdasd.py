import easygui as gui
import sys

users = {}
status = ""

def displayMenu():
    status = gui.buttonbox("Are you a registered user?", "Login", choices=('Yes', 'No', 'Cancel'))  
    if status == 'Yes':
        oldUser()
    elif status == 'No':
        newUser()
    elif status == 'Cancel':
        sys.exit(0)

def newUser():
    createLogin = gui.enterbox("Create login name")
    if createLogin in users:
        gui.msgbox("Login name already exists!")
    else:
        createPassw = gui.enterbox("Create password")
        users[createLogin] = createPassw
        gui.msgbox("User created!")
        displayMenu()

def oldUser():
    login = gui.enterbox("Enter login name")
    passw = gui.enterbox("Enter password")
    logintries = 1

    if login in users and users[login] == passw: 
        gui.msgbox("Login successful!")
    else:
        while logintries < 5:
            gui.msgbox("User doesn't exist or wrong password!")
            login = gui.enterbox("Enter login name")
            passw = gui.enterbox("Enter password")
            logintries = logintries + 1
        gui.msgbox("Maximum amount of login tries reached.")

#while status != "q":            
displayMenu()